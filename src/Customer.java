package src;


public class Customer {
	
	private int id;
	private String name;
	private String password;
	private String address;
	private String email;
	private String phoneNumber;
	private int age;
	
	void setId(int custId) {
		id = custId;
	}
	
	void setName(String custName) {
		name = custName;
	}
	void setPass(String custPass) {
		password = custPass;
	}
	
	void setAddress(String custAddr) {
		address = custAddr;
	}
	void setEmail(String custEmail) {
		email = custEmail;
	}
	
	void setPhoneNumber(String custPhone) {
		phoneNumber = custPhone;
	}
	void setAge(int custAge) {
		age = custAge;
	}
	
	int getId() {
		return id;
	}
	
	String getName() {
		return name;
	}
	String getPassword() {
		return password;
	}
	
	String getAddress() {
		return address;
	}
	String getEmail() {
		return email;
	}
	
	String getPhoneNumber() {
		return phoneNumber;
		
	}
	int getAge() {
		return age;
	}
	
	public Customer(String custName,String custPass, String custAddr, String custEmail, String custPhone, int custAge) throws CustomerExceptionHandler  {
		
		id = 0;
		
		// Validate Input//
		try {
			
			validateName(custName);
			validatePass(custPass);
			validateAddress(custAddr);
			validateEmail(custEmail);
			validatePhoneNumber(custPhone);
			validateAge(custAge);
			
		}
		catch (CustomerExceptionHandler e) {
			throw e;
		}
		
		// Set Attributes
		name = custName;
		password = custPass;
		address = custAddr;
		email = custEmail;
		phoneNumber = custPhone;
		age = custAge;
	}
	
	public static void validateName(String custName) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer Name"
		//E.G. Name String must be a minimum of 2 characters and a maximum of 50 characters
		
		if (custName.isBlank() || custName.isEmpty())
			throw new CustomerExceptionHandler("Customer Name NOT specified");
		else if (custName.length() < 2)
			throw new CustomerExceptionHandler("Customer Name does not meet minimum length requirements");
		else if (custName.length() > 50)
			throw new CustomerExceptionHandler("Customer Name does not exceeds maximum length requirements");
		
	}
     public static void validatePass(String custPass) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer Name"
		//E.G. Name String must be a minimum of 2 characters and a maximum of 50 characters
		
		if (custPass.isBlank() || custPass.isEmpty())
			throw new CustomerExceptionHandler("Customer Password NOT specified");
		else if (custPass.length() < 8)
			throw new CustomerExceptionHandler("Customer Password does not meet minimum length requirements");
		else if (custPass.length() > 14)
			throw new CustomerExceptionHandler("Customer Password does not exceeds maximum length requirements");
		
	}
	
	
	public static void validateAddress(String custAddr) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer Address"
		//E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters
		
		if (custAddr.isBlank() || custAddr.isEmpty())
			throw new CustomerExceptionHandler("Customer Address NOT specified");
		else if (custAddr.length() < 5)
			throw new CustomerExceptionHandler("Customer Address does not meet minimum length requirements");
		else if (custAddr.length() > 70)
			throw new CustomerExceptionHandler("Customer Address does not exceeds maximum length requirements");
		
	}
public static void validateEmail(String custEmail) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer custEmail"
		//E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters
		
		if (custEmail.isBlank() || custEmail.isEmpty())
			throw new CustomerExceptionHandler("Customer Email NOT specified");
		else if (custEmail.length() < 5)
			throw new CustomerExceptionHandler("Customer Email does not meet minimum length requirements");
		else if (custEmail.length() > 25)
			throw new CustomerExceptionHandler("Customer Email does not exceeds maximum length requirements");
		
	}

	
	public static void validatePhoneNumber(String custPhone) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer PhoneNumber"
		//E.G. Name String must be a minimum of 7 characters and a maximum of 15 characters
		
		if (custPhone.isBlank() || custPhone.isEmpty())
			throw new CustomerExceptionHandler("Customer PhoneNumber NOT specified");
		else if (custPhone.length() < 7)
			throw new CustomerExceptionHandler("Customer PhoneNumber does not meet minimum length requirements");
		else if (custPhone.length() > 15)
			throw new CustomerExceptionHandler("Customer PhoneNumber does not exceeds maximum length requirements");
		
	}
	
public static void validateAge(int custAge) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer PhoneNumber"
		//E.G. Name String must be a minimum of 7 characters and a maximum of 15 characters
		
		if (custAge == 0)
			throw new CustomerExceptionHandler("Customer Age NOT specified");
		else if (custAge < 18)
			throw new CustomerExceptionHandler("Customer Age does not meet minimum requirements");
		else if (custAge > 100)
			throw new CustomerExceptionHandler("Customer Age does not exceeds maximum requirements");
		
	}

}
