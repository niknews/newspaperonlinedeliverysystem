package src;


import junit.framework.TestCase;

public class SubscriptionTest extends TestCase {
	
	//Test #: 1
	//Test Objective: To create a Subscriber Account
	//Inputs: custName = "Jack Daniels", custAddr = "Athlone", custPhone = "087-123123123"
	//Expected Output: Customer Object created with id = 0, "Jack Daniels", custAddr = "Athlone", custPhone = "087-123123123"
	
	public void testSubscription001() {
		
		//Create the Subscriber Object
		
		
		try {
			
			//Call method under test
			Subscription subObj = new Subscription("Jack Daniels", "Athlone", "087-123123123","ait@gmail.com", "11223344", 18 );
			
			// Use getters to check for object creation
			assertEquals(0, subObj.getId());
			assertEquals("Jack Daniels", subObj.getName());
			assertEquals("Athlone", subObj.getAddress());
			assertEquals("087-123123123", subObj.getPhoneNumber());
			assertEquals("ait@gmail.com", subObj.getEmail());
			assertEquals("11223344", subObj.getPassword());
			assertEquals(18, subObj.getAge());
		}
		catch (SubscriptionExceptionHandler e) {
			fail("Exception not expected");
		}
		
	}
	
	//Test #: 2
	//Test Objective: To catch an invalid Subscriber name
	//Inputs: custName = "J"
	//Expected Output: Exception Message: "Subscriber Name does not meet minimum length requirements"

	public void testValidateName001() {
			
		try {
				
			//Call method under test
			Subscription.validateName("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Customer Name does not meet minimum length requirements", e.getMessage());	
		}
	}
	//Test #: 3
	//Test Objective: To catch an invalid subscription address 
	//Inputs: subAddr = "J"
	//Expected Output: Exception Message: "Subscribers Address does not meet minimum length requirements"

	public void testValidateAddress002() {
			
		try {
				
			//Call method under test
			Subscription.validateAddress("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Customer Address does not meet minimum length requirements", e.getMessage());	
		}
	}
	//Test #: 4
	//Test Objective: To catch an invalid subscription phone number
	//Inputs: subPhone = "J"
	//Expected Output: Exception Message: "Customer Name does not meet minimum length requirements"

	public void testValidatePhoneNumber003() {
			
		try {
				
			//Call method under test
			Subscription.validatePhoneNumber("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Subscribers Phone number does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	//Test #: 5
	//Test Objective: To catch an invalid subscription password
	//Inputs: subPass = "J"
	//Expected Output: Exception Message: "Subscription password does not meet minimum length requirements"

	public void testValidatePassword004() {
			
		try {
				
			//Call method under test
			Subscription.validatePassword("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Subscription password does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	//Test #: 6
	//Test Objective: To catch an invalid subscription email
	//Inputs: subPass = "J"
	//Expected Output: Exception Message: "Subscription email does not meet minimum length requirements"

	public void testValidateEmail005() {
			
		try {
				
			//Call method under test
			Subscription.validateEmail("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Subscription email does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	
	//Test #: 7
	//Test Objective: To catch an invalid subscription age
	//Inputs: subAge = "J"
	//Expected Output: Exception Message: "Subscription age does not meet minimum length requirements"

	public void testValidateAge006() {
			
		try {
				
			//Call method under test
			Subscription.validateAge(18);
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Subscription age does not meet minimum length requirements", e.getMessage());	
		}
	}

}

		

=======
//import junit.framework.TestCase;

//public class SubscriptionTest extends TestCase {

//}

import junit.framework.TestCase;

public class SubscriptionTest extends TestCase {
	
	//Test #: 1
	//Test Objective: To create a Subscriber Account
	//Inputs: custName = "Jack Daniels", custAddr = "Athlone", custPhone = "087-123123123"
	//Expected Output: Customer Object created with id = 0, "Jack Daniels", custAddr = "Athlone", custPhone = "087-123123123"
	
	public void testSubscription001() {
		
		//Create the Subscriber Object
		
		
		try {
			
			//Call method under test
			Subscription subObj = new Subscription("Jack Daniels", "Athlone", "087-123123123","ait@gmail.com", "11223344", 18 );
			
			// Use getters to check for object creation
			assertEquals(0, subObj.getId());
			assertEquals("Jack Daniels", subObj.getName());
			assertEquals("Athlone", subObj.getAddress());
			assertEquals("087-123123123", subObj.getPhoneNumber());
			assertEquals("ait@gmail.com", subObj.getEmail());
			assertEquals("11223344", subObj.getPassword());
			assertEquals(18, subObj.getAge());
		}
		catch (SubscriptionExceptionHandler e) {
			fail("Exception not expected");
		}
		
	}
	
	//Test #: 2
	//Test Objective: To catch an invalid Subscriber name
	//Inputs: custName = "J"
	//Expected Output: Exception Message: "Subscriber Name does not meet minimum length requirements"

	public void testValidateName001() {
			
		try {
				
			//Call method under test
			Subscription.validateName("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Customer Name does not meet minimum length requirements", e.getMessage());	
		}
	}
	//Test #: 3
	//Test Objective: To catch an invalid subscription address 
	//Inputs: subAddr = "J"
	//Expected Output: Exception Message: "Subscribers Address does not meet minimum length requirements"

	public void testValidateAddress002() {
			
		try {
				
			//Call method under test
			Subscription.validateAddress("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Customer Address does not meet minimum length requirements", e.getMessage());	
		}
	}
	//Test #: 4
	//Test Objective: To catch an invalid subscription phone number
	//Inputs: subPhone = "J"
	//Expected Output: Exception Message: "Customer Name does not meet minimum length requirements"

	public void testValidatePhoneNumber003() {
			
		try {
				
			//Call method under test
			Subscription.validatePhoneNumber("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Subscribers Phone number does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	//Test #: 5
	//Test Objective: To catch an invalid subscription password
	//Inputs: subPass = "J"
	//Expected Output: Exception Message: "Subscription password does not meet minimum length requirements"

	public void testValidatePassword004() {
			
		try {
				
			//Call method under test
			Subscription.validatePassword("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Subscription password does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	//Test #: 6
	//Test Objective: To catch an invalid subscription email
	//Inputs: subPass = "J"
	//Expected Output: Exception Message: "Subscription email does not meet minimum length requirements"

	public void testValidateEmail005() {
			
		try {
				
			//Call method under test
			Subscription.validateEmail("J");
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Subscription email does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	
	//Test #: 7
	//Test Objective: To catch an invalid subscription age
	//Inputs: subAge = "J"
	//Expected Output: Exception Message: "Subscription age does not meet minimum length requirements"

	public void testValidateAge006() {
			
		try {
				
			//Call method under test
			Subscription.validateAge(18);
			fail("Exception expected");
		}
		catch (SubscriptionExceptionHandler e) {
			assertEquals("Subscription age does not meet minimum length requirements", e.getMessage());	
		}
	}

}

