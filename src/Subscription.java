package src;




public class Subscription {

	private int id;
	private int age;
	private String email;
	private String password;
	private String name;
	private String address;
	private String phoneNumber;
	
	void setId(int subId) {
		id = subId;
	}
	void setAge(int subAge) {
		age = subAge;
	}
	void setEmail(String subEmail) {
		email = subEmail;
	}
	void setPassword(String subPass) {
		password = subPass; 
	}
	
	void setName(String subName) {
		name = subName;
	}
	
	void setAddress(String subAddr) {
		address = subAddr;
	}
	
	void setPhoneNumber(String subPhone) {
		phoneNumber = subPhone;
	}
	
	int getId() {
		return id;
	}
	int getAge() {
		return age;
	}
	String getEmail() {
		return email;
	}
	String getPassword() {
		return password;
	}
	
	String getName() {
		return name;
	}
	
	String getAddress() {
		return address;
	}
	
	String getPhoneNumber() {
		return phoneNumber;
		
	}
	
	public Subscription(String subName, String subAddr, String subPhone, String subEmail, String subPass, int subAge) throws SubscriptionExceptionHandler  {
		
		id = 0;
		
		// Validate Input
		try {
			
			validateName(subName);
			validateAddress(subAddr);
			validatePhoneNumber(subPhone);
			validateEmail(subEmail);
			validatePassword(subPass);
			validateAge(subAge);
			
		}
		catch (SubscriptionExceptionHandler e) {
			throw e;
		}
		
		// Set Attributes
		name = subName;
		address = subAddr;
		phoneNumber = subPhone;
		email = subEmail;
		age = subAge;
		password = subPass;
	}
	
	public static void validateAge(int subAge) throws SubscriptionExceptionHandler {
		
		//Agree Formating Rules on "Subscriber Age"
		//E.G. Age must be over 18 and less than 100
		
		if (subAge == 0)
			throw new SubscriptionExceptionHandler("Subscriber Age NOT specified");
		else if (subAge < 18)
			throw new SubscriptionExceptionHandler("Subscriber Age does not meet minimum requirements");
		else if (subAge > 100)
			throw new SubscriptionExceptionHandler("Subscriber Age exceeds maximum requirements");
		
	}
	
	public static void validatePassword(String subPass) throws SubscriptionExceptionHandler {
		
		//Agree Formating Rules on "Subscriber Password"
		//E.G. Password String must be a minimum of 8 characters and a maximum of 14 characters
		
		if (subPass.isBlank() || subPass.isEmpty())
			throw new SubscriptionExceptionHandler("Subscriber Password NOT specified");
		else if (subPass.length() < 8)
			throw new SubscriptionExceptionHandler("Subscriber Password does not meet minimum length requirements");
		else if (subPass.length() > 14)
			throw new SubscriptionExceptionHandler("Subscriber Password exceeds maximum length requirements");
		
	}
	
	public static void validateEmail(String subEmail) throws SubscriptionExceptionHandler {
		
		//Agree Formating Rules on "Subscriber Email"
		//E.G. Email String must be a minimum of 5 characters and a maximum of 25 characters
		
		if (subEmail.isBlank() || subEmail.isEmpty())
			throw new SubscriptionExceptionHandler("Subscriber Email NOT specified");
		else if (subEmail.length() < 5)
			throw new SubscriptionExceptionHandler("Subscriber Email does not meet minimum length requirements");
		else if (subEmail.length() > 25)
			throw new SubscriptionExceptionHandler("Subscriber Email exceeds maximum length requirements");
		
	}
	
	public static void validateName(String subName) throws SubscriptionExceptionHandler {
		
		//Agree Formating Rules on "Subscriber Name"
		//E.G. Name String must be a minimum of 2 characters and a maximum of 50 characters
		
		if (subName.isBlank() || subName.isEmpty())
			throw new SubscriptionExceptionHandler("Subscriber Name NOT specified");
		else if (subName.length() < 2)
			throw new SubscriptionExceptionHandler("Subscriber Name does not meet minimum length requirements");
		else if (subName.length() > 50)
			throw new SubscriptionExceptionHandler("Subscriber Name does not exceeds maximum length requirements");
		
	}
	
	public static void validateAddress(String subAddr) throws SubscriptionExceptionHandler {
		
		//Agree Formating Rules on "Subscriber Address"
		//E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters
		
		if (subAddr.isBlank() || subAddr.isEmpty())
			throw new SubscriptionExceptionHandler("Subscriber Address NOT specified");
		else if (subAddr.length() < 5)
			throw new SubscriptionExceptionHandler("Subscriber Address does not meet minimum length requirements");
		else if (subAddr.length() > 60)
			throw new SubscriptionExceptionHandler("Subscriber Address does not exceeds maximum length requirements");
		
	}
	
	public static void validatePhoneNumber(String subPhone) throws SubscriptionExceptionHandler {
		
		//Agree Formating Rules on "Subscriber PhoneNumber"
		//E.G. Name String must be a minimum of 7 characters and a maximum of 15 characters
		
		if (subPhone.isBlank() || subPhone.isEmpty())
			throw new SubscriptionExceptionHandler("Subscriber PhoneNumber NOT specified");
		else if (subPhone.length() < 7)
			throw new SubscriptionExceptionHandler("Subscriber PhoneNumber does not meet minimum length requirements");
		else if (subPhone.length() > 15)
			throw new SubscriptionExceptionHandler("Subscriber PhoneNumber does not exceeds maximum length requirements");
		
	}
	

}

