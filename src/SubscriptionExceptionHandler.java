package src;


public class SubscriptionExceptionHandler extends Exception {

	String message;
	
	public SubscriptionExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}
}