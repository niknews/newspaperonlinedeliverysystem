package src;

//import junit.framework.TestCase;

//public class CustomerTest extends TestCase {

//}

import junit.framework.TestCase;

public class CustomerTest extends TestCase {
	
	//Test #: 1
	//Test Objective: To create a Customer Account
	//Inputs: custName = "Jack Daniels", custAddr = "Athlone", custPhone = "087-123123123"
	//Expected Output: Customer Object created with id = 0, "Jack Daniels", custAddr = "Athlone", custPhone = "087-123123123"
	
	public void testCustomer001() {
		
		//Create the Customer Object..
		
		
		try {
			
			//Call method under test
			Customer custObj = new Customer("Nikhil Donde","Java@260551", "Athlone", "087-169842255","nikd@gmail.com", 18);
			
			// Use getters to check for object creation
			assertEquals(0, custObj.getId());
			assertEquals("Nikhil Donde", custObj.getName());
			assertEquals("Athlone", custObj.getAddress());
			assertEquals("Java@260551", custObj.getPassword());
			assertEquals("087-169842255", custObj.getPhoneNumber());
			assertEquals("nikd@gmail.com", custObj.getEmail());
			assertEquals(18, custObj.getAge());
		}
		catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
		
	}
	
	//Test #: 2
	//Test Objective: To catch an invalid customer name
	//Inputs: custName = "J"
	//Expected Output: Exception Message: "Customer Name does not meet minimum length requirements"

	public void testValidateName001() {
			
		try {
				
			//Call method under test
			Customer.validateName("J");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e) {
			assertEquals("Customer Name does not meet minimum length requirements", e.getMessage());	
		}
	}

	//Test #: 3
		//Test Objective: To catch an invalid customer password
		//Inputs: custPass = "J"
		//Expected Output: Exception Message: "Customer Password does not meet minimum length requirements"

		public void testValidatePassword002() {
				
			try {
					
				//Call method under test
				Customer.validatePass("J");
				fail("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer password does not meet minimum length requirements", e.getMessage());	
			}
		}
		
		//Test #: 4
		//Test Objective: To catch an invalid customer address
		//Inputs: custAddress = "J"
		//Expected Output: Exception Message: "Customer Address does not meet minimum length requirements"

		public void testValidateAddress003() {
				
			try {
					
				//Call method under test
				Customer.validateAddress("J");
				fail("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer address does not meet minimum length requirements", e.getMessage());	
			}
		}

		//Test #: 5
		//Test Objective: To catch an invalid customer phone number
		//Inputs: custEmail = "J"
		//Expected Output: Exception Message: "Customer Phone Number does not meet minimum length requirements"

		public void testValidatePhoneNumber004() {
				
			try {
					
				//Call method under test
				Customer.validatePhoneNumber("J");
				fail("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer Phone Number does not meet minimum length requirements", e.getMessage());	
			}
		}

		//Test #: 6
		//Test Objective: To catch an invalid customer email
		//Inputs: custEmail = "J"
		//Expected Output: Exception Message: "Customer Email does not meet minimum length requirements"

		public void testValidateEmail005() {
				
			try {
					
				//Call method under test
				Customer.validateEmail("J");
				fail("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer Email does not meet minimum length requirements", e.getMessage());	
			}
		}

		//Test #: 7
		//Test Objective: To catch an invalid customer age
		//Inputs: custName = "J"
		//Expected Output: Exception Message: "Customer Age does not meet minimum length requirements"

		public void testValidateAge006() {
				
			try {
					
				//Call method under test
				Customer.validateAge(5);
				fail("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer Age does not meet minimum length requirements", e.getMessage());	
			}
		}


}

		


