package src;

//import junit.framework.TestCase;

//public class CustomerTest extends TestCase {

//}

import junit.framework.TestCase;

public class CustomerTest2 extends TestCase {
	
	//Test #: 1
	//Test Objective: To create a Customer Account
	//Inputs: custName = "Nikhil Donde", custAddr = "Athlone", custPhone = "087-123123123"
	//Expected Output: Customer Object created with id = 0, "Jack Daniels", custAddr = "Athlone", custPhone = "087-123123123"
	
	public void testCustomer001() {
		
		//Create the Customer Object..
		
		
		try {
			
			//Call method under test
			Customer custObj = new Customer("Nikhil Donde","Java260551", "Athlone", "087-1698422", "nikd@gmail.com", 18);
			
			// Use getters to check for object creation
			assertEquals(0, custObj.getId());
			assertEquals("Nikhil Donde", custObj.getName());
			assertEquals("Java260551", custObj.getPassword());
			assertEquals("Athlone", custObj.getAddress());
		//	assertEquals("087-1698422", custObj.getPhoneNumber());
		//	assertEquals("nikd@gmail.com", custObj.getEmail());
			assertEquals(18, custObj.getAge());
			
		}
		catch (CustomerExceptionHandler e) {
			pass("Exception expected");
		}
		
	}
	
	//Test #: 2
	//Test Objective: To catch an valid customer name
	//Inputs: custName = "Nikhil Donde"
	//Expected Output: Exception Message: "Customer Name meet all requirements"

	private void pass(String string) {
		// TODO Auto-generated method stub
		
	}

	public void testValidateName001() {
			
		try {
				
			//Call method under test
			Customer.validateName("Nikhil Donde");
			pass("Exception expected");
		}
		catch (CustomerExceptionHandler e) {
			assertEquals("Customer Name meet all requirements", e.getMessage());	
		}
	}

	//Test #: 3
		//Test Objective: To catch an valid customer password
		//Inputs: custPass = "Java@260551"
		//Expected Output: Exception Message: "Customer password matches length requirements"

		public void testValidatePassword002() {
				
			try {
					
				//Call method under test
				Customer.validatePass("Java@260551");
				pass("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer password matches length requirements", e.getMessage());	
			}
		}
		
		//Test #: 4
		//Test Objective: To catch an valid customer address
		//Inputs: custAddress = "Athlone"
		//Expected Output: Exception Message: "Customer Address matches  requirements"

		public void testValidateAddress003() {
				
			try {
					
				//Call method under test
				Customer.validateAddress("Athlone");
				pass("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer Address matches  requirements", e.getMessage());	
			}
		}

		//Test #: 5
		//Test Objective: To catch an valid customer phone number
		//Inputs: custPhoneNumber = "087-169842255"
		//Expected Output: Exception Message: "Customer Phone Number meets the requirements"

			public void testValidatePhoneNumber004() {
						
					try {
							
						//Call method under test
					Customer.validatePhoneNumber("087-1698422k");
						pass("Exception expected");
					}
					catch (CustomerExceptionHandler e) {
						assertEquals("Customer Phone Number meets the requirements", e.getMessage());	
					}
				}//

		//Test #: 6
		//Test Objective: To catch an valid customer email
		//Inputs: custEmail = "nikd@gmail.com"
		//Expected Output: Exception Message: "Customer Email meets the requirements"

		public void testValidateEmail005() {
				
		try {
					
				//Call method under test
			    Customer.validateEmail("nikd@gmail.com");
				pass("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer Email meets the requirements", e.getMessage());	
			}
		}

		//Test #: 7
		//Test Objective: To catch an valid customer age
		//Inputs: custAge = "18"
		//Expected Output: Exception Message: "Customer Age meets minimum requirements"

		public void testValidateAge006() {
				
			try {
					
				//Call method under test
				Customer.validateAge(18);
				pass("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				assertEquals("Customer Age meets minimum requirements", e.getMessage());	
			}
		}


}

		


